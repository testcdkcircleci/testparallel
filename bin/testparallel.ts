#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { TestparallelStack } from '../lib/testparallel-stack';

const app = new cdk.App();
new TestparallelStack(app, 'TestparallelStack');
